const mix = require('laravel-mix');

mix.react('resources/js/app/index.js', 'public/js/app.js');
mix.sass('resources/sass/app.scss', 'public/css');

if (mix.inProduction())
    mix.version();
else
    mix.sourceMaps();
