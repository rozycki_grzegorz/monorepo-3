# Monorepo 3

## How to run:

Follow these steps to install and run the application:

1. after pull cd into main directory
2. run `composer install`
3. run `yarn install`
4. run `yarn prod`
5. run `php artisan serve`
6. open http://localhost:8000 in a browser

If you're running on Windows you may encounter errors.
This is probably caused by composer post install script failing.
Try these steps:

1. copy the `.env.example` file as `.env`
2. run `php artisan key:generate`
3. retry steps 5, 6 from previous instruction

## What's going on?

- `yarn workspaces` are used for monorepo / cross-package management,
- `laravel-mix` is used for build process.

### TODO

- watch / dev was not tested,
- include of `create-react-app` created projects was not tested,
- running projects from subdirectories was not tested.
