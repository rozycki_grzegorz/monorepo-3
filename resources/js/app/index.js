import {App} from '@grit/components';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
    <App/>,
    document.getElementById('app')
);
