<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
    </style>
</head>
<body id="app">
<noscript>
    @lang('app.javascript_required')
</noscript>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
